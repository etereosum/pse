<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\PseProvider;
use App\Transaction;

class PagoController extends Controller
{
    /*
    |--------------------------------------------------
    | create
    |--------------------------------------------------
    |
    |   carga el formulario de recolección de datos
    |   del payer, como datos personales, banco y 
    |   tipo de banca
    |   Retorna: view pagos.create
    |
    |   creó: PG 06-12-17
    */

    public function create(){ 

        return view('pagos.create',[
            'banks'             => getBanksList(),
            'bankInterfaces'    => getBankInterfaces(),
            'documentTypes'     => getDocumentTypes()
            ]);
    
    }

    /*
    |--------------------------------------------------
    | create
    |--------------------------------------------------
    |
    |   con la información recolectada de pagos.create
    |   1) valida la data
    |   2) crea la transaccíon
    |   3) evalua el estado de esa transacción para dar 
    |      respuesta.
    |   Retorna: si la transacción se creó exitosamente
    |               crea un registro en base de datos
    |               si no, muestra un mensaje de error
    |   creó: PG 06-12-17
    */


    public function transaction(Request $request){  

        //***** Start validations ********//

        $rules      = array ('bankCode' => 'required','bankInterface' => 'required' );

        if($request->input('payerDefault')){
            $this->validate($request,$rules);   
            $payer = getPayerDefault();   
        }
        else{
            $rulesPayer = array(
                'firstName'     => 'required|max:60',
                'lastName'      => 'required|max:60',
                'documentType'  => 'required|max:3',
                'document'      => 'required|max:12',
                'company'       => 'required|max:60',
                'emailAddress'  => 'required|max:80|email',
                'province'      => 'required|max:50',
                'city'          => 'required|max:50',
                'address'       => 'required|max:100',
                'phone'         => 'required|max:30',
                'mobile'        => 'required|max:30'
            );

            $rules = array_merge($rules,$rulesPayer);

            $this->validate($request,$rules);

            $payer = [
                'firstName'     => $request->input('firstName'),
                'lastName'      => $request->input('lastName'),
                'documentType'  => $request->input('documentType'),
                'document'      => $request->input('document'),
                'company'       => $request->input('company'),
                'emailAddress'  => $request->input('emailAddress'),
                'country'       => 'CO',
                'province'      => $request->input('province'),
                'city'          => $request->input('city'),
                'address'       => $request->input('address'),
                'phone'         => $request->input('phone'),
                'mobile'        => $request->input('mobile')
            ];
        } //end else

        //***** End validations ********//


        $resulTransaction = createTransaction(
                                        $request->input('bankCode'),
                                        $request->input('bankInterface'),
                                        1000,
                                        0,
                                        0,
                                        0,
                                        $payer
                                    );

        if($resulTransaction->returnCode == 'SUCCESS' ){
            
            $transaction = new Transaction();
            
            $transaction->documentPayer         =  $payer['document'];
            $transaction->bankCode              =  $request->input('bankCode');
            $transaction->bankInterface         =  $request->input('bankInterface');
            $transaction->returnCode            =  $resulTransaction->returnCode;
            $transaction->bankUrl               =  $resulTransaction->bankURL;
            $transaction->trazabilityCode       =  $resulTransaction->trazabilityCode;
            $transaction->transactionCycle      =  $resulTransaction->transactionCycle;
            $transaction->transactionID         =  $resulTransaction->transactionID;
            $transaction->sessionID             =  $resulTransaction->sessionID;
            $transaction->bankCurrency          =  $resulTransaction->bankCurrency;
            $transaction->bankFactor            =  $resulTransaction->bankFactor;
            $transaction->responseCode          =  $resulTransaction->responseCode;
            $transaction->responseReasonCode    =  $resulTransaction->responseReasonCode;
            $transaction->responseReasonText    =  $resulTransaction->responseReasonText;

            $transaction->save();

           return redirect( $resulTransaction->bankURL );

        }
        else{
            flash()->error($resulTransaction->responseReasonText);
            return redirect()->route('pagos.create');
        }
    }

    /*
    |--------------------------------------------------
    | index
    |--------------------------------------------------
    |
    |   Se carga la vista donde se va mostrará la lista de 
    |   transacciones almacenadas, esa lista se trae 
    |   mediante ajax con la funcion listTransactions
    |   Retorna: view pagos.index
    |
    |   creó: PG 06-12-17
    */


    public function index(){
          return view( 'pagos.index');
    }

    /*
    |--------------------------------------------------
    | lisTransactions
    |--------------------------------------------------
    |
    |   funcion que retorna un json con el listado de todas 
    |   las transacciones almacenadas en la base de datos 
    |   ordenadas descendentemente por id
    |
    |   creó: PG 06-12-17
    */


    public function listTransactions(){

        try{
            // confirma si alguna de las transacciones fue aprobadas

            $this->confirm(); 
        
                //lista las transacciones y las envia a la vista
        
                $transactions = Transaction::orderBy('id','desc')->get();
                return response()->json(['error' => false,'data'  => $transactions]);
        }
        catch(\Exception $e){
            return response()->json(['error' => true, 'data' => $e]);
        }
    }

    /*
    |--------------------------------------------------
    | confirm
    |--------------------------------------------------
    |
    |   Actualiza c/u de las transations cuyo atributo 
    |   returnCode cambie de 'SUCCESS' a 'OK' 
    |   
    |   creó: PG 06-12-17
    */
    
    public function confirm(){

        try{
            $transactionsWait  = Transaction::where('returnCode','SUCCESS')->get();

            foreach($transactionsWait as $transaction){

                $info = getTransactionInformation($transaction->transactionID);

                if($info->getTransactionInformationResult->transactionState == 'OK'){
                    $transaction->returnCode            = 'OK';
                    $transaction->responseReasonText    = 'Operacion Aprobada';
                    $transaction->save();
                }
            }//end foreach
        }//end try
        catch(\Exception $e){
            dd('function confirm :'.$e);
        }

    }
}
