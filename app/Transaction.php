<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [  
              
        'documentPayer',
        'returnCode',
        'bankCode',
        'bankInterface',
        'bankUrl',
        'trazabilityCode',
        'transactionCycle',
        'transactionID',
        'sessionID',
        'bankCurrency',
        'bankFactor',
        'responseCode',
        'responseReasonCode',
        'responseReasonText'
    ];
}
