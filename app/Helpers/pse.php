<?php

namespace App\Http\Controllers;

/*
|--------------------------------------------------
| getAuth
|--------------------------------------------------
|
|   Retorna las credenciales de acceso al web service
|   creó: PG 05-12-17
*/

function getAuth(){
    $seed           = date('c');
    $tranKey        = '024h1IlD';
    $hash           = sha1($seed . $tranKey, false);

    $authentication = array(
        'login'     => '6dd490faf9cb87a9862245da41170ff2',
        'tranKey'   => $hash,
        'seed'      => $seed
    );
    $auth =  (object) $authentication;

    return $auth;
}

/*
|--------------------------------------------------
| getAgetClientSoaputh
|--------------------------------------------------
|
|   Retorna el objeto del servicio con el cual
|   se pueden llamar los diferentes metodos como
|   crear transaccion, consultar información etc.
|   creó: PG 05-12-17
|
*/

function getClientSoap(){
    $url            = 'https://test.placetopay.com/soap/pse/?wsdl';
    $option         = ['trace' => true];
    $soapClient     = new \SoapClient($url,$option);   

    $soapClient->__setLocation('https://test.placetopay.com/soap/pse/');

    return $soapClient;
}

/*
|--------------------------------------------------
| getBanksList
|--------------------------------------------------
|
|   Retorna un array de bancos para realizar
|   el debito.
|   
|   Se utilizala \Cache de laravel para almacenar
|   el listado durante 24 horas
|
|   creó: PG 05-12-17
|
*/

function getBanksList(){

        // Si el listado de bancos esta en cache retornelos
    
        if( \Cache::get('listBanksCache') ){
            $listBanks      = \Cache::get( 'listBanksCache' );
        }

        // Si el listado no esta en cache solicitelos al web service y guarde en cache

        else{
            $soapClient    = getClientSoap();
            $objBanks      = $soapClient->getBankList( ['auth' => getAuth()]);
            $listBanks     = $objBanks->getBankListResult->item;
    
            \Cache::put('listBanksCache',$listBanks,1440);
        }
    
        return $listBanks;
    }

/*
|--------------------------------------------------
| createTransaction
|--------------------------------------------------
|
|   Se envía la data requerida al web service 
|   para generar una transacción
|   
|   Recibe: $bakCode        = código del banco
|           $bankInterface  = 0 para banca personas
|                             1 para banca empresas
|           $totalAmount    = valor a debitar (por default 1000)
|           $taxAmount, $devolutionBase,$tipAmount = valores en 0
|           $payer          = informacion del payer
|
|   Retorna: un objeto con la información provista
|   por el web service.
|
|   creó: PG 05-12-17
*/

function createTransaction($bankCode,$bankInterface,$totalAmount, $taxAmount, $devolutionBase, $tipAmount, $payer ){
    
        $transactionData = [
            'bankCode'      =>  $bankCode,
            'bankInterface' =>  $bankInterface,
            'returnURL'     =>  url('pagos/index'), 
            'reference'     =>  md5(date('dmYHis')),
            'description'   =>  'Prueba',
            'language'      =>  'ES',
            'currency'      =>  'COP',
            'totalAmount'   =>  $totalAmount,
            'taxAmount'     =>  $taxAmount,
            'devolutionBase'=>  $devolutionBase,
            'tipAmount'     =>  $tipAmount,
            'payer'         =>  $payer,
            'buyer'         =>  getBuyer(),
            'shipping'      =>  getShipping(), 
            'ipAddress'     => $_SERVER['REMOTE_ADDR'],
            'userAgent'     => $_SERVER['HTTP_USER_AGENT']
        ];
    
        $soapClient     = getClientSoap();
    
        $result = $soapClient->createTransaction(['auth' => getAuth(), 'transaction' => $transactionData]);
    
        return $result->createTransactionResult;
    }

/*
|--------------------------------------------------
| getPayerDefault
|--------------------------------------------------
|
|   Payer utilizado por defecto 
|   Retorna: array con la info del payer
|   creó: PG 05-12-17
*/

function getPayerDefault(){
    $payer         =  [
        'document'      => '9873241',
        'documentType'  => 'CC',
        'firstName'     => 'Pablo',
        'lastName'      => 'Gonzalez',
        'company'       => 'Mia',
        'emailAddress'  => 'etereosum@gmail.com',
        'address'       => 'cra 37a # 29-41',
        'city'          => 'Pereira',
        'province'      => 'Risaralda',
        'country'       => 'CO',
        'phone'         => '3163033',
        'mobile'        => '3207809668'
    ];

    return $payer;
}

/*
|--------------------------------------------------
| getBuyer
|--------------------------------------------------
|
|   Buyer utilizado en todas las transacciones
|   Retorna: array con la info del buyer
|   creó: PG 05-12-17
*/

function getBuyer(){
    $buyer = [
        'document'      => '9873241',
        'documentType'  => 'CC',
        'firstName'     => 'Pablo',
        'lastName'      => 'Gonzalez',
        'company'       => 'Mia',
        'emailAddress'  => 'etereosum@gmail.com',
        'address'       => 'cra 37a # 29-41',
        'city'          => 'Pereira',
        'province'      => 'Risaralda',
        'country'       => 'CO',
        'phone'         => '3163033',
        'mobile'        => '3207809668'
    ];
    return $buyer;
}

/*
|--------------------------------------------------
| getShipping
|--------------------------------------------------
|
|   Shipping utilizado en todas las transacciones
|   Retorna: array con la info el shipping
|   creó: PG 04-12-17
*/

function getShipping(){
    $shipping     =  [
        'document'      => '9873241',
        'documentType'  => 'CC',
        'firstName'     => 'Pablo',
        'lastName'      => 'Gonzalez',
        'company'       => 'Mia',
        'emailAddress'  => 'etereosum@gmail.com',
        'address'       => 'cra 37a # 29-41',
        'city'          => 'Pereira',
        'province'      => 'Risaralda',
        'country'       => 'CO',
        'phone'         => '3163033',
        'mobile'        => '3207809668'
    ];
    return $shipping;
}

/*
|--------------------------------------------------
| getDocumentTypes
|--------------------------------------------------
|
|   Retorna: array con los tipos de documento
|   creó: PG 04-12-17
*/


function getDocumentTypes(){
    $documentTypes = array(
        ['abbreviation' => 'CC',    'extended' => 'Cédula de ciudanía colombiana'],
        ['abbreviation' => 'CE',    'extended' => 'Cédula de extranjería'],
        ['abbreviation' => 'TI',    'extended' => 'Tarjeta de identidad'],
        ['abbreviation' => 'PPN',   'extended' => 'Pasaporte']
    );

    return $documentTypes;
}

/*
|--------------------------------------------------
| getBankInterfaces
|--------------------------------------------------
|
|   Retorna: array con los tipos de interface
|   0 = Personas , 1 = Empresas
|   creó: PG 05-12-17
*/


function getBankInterfaces(){

    $bankInterfaces = array(
        ['interface' => 'Personas' , 'interfaceCode' => 0],
        ['interface' => 'Empresas' , 'interfaceCode' => 1]
    );

    return $bankInterfaces;
}

/*
|--------------------------------------------------
| getTransactionInformation
|--------------------------------------------------
|
|   Retorna: objeto con la informacion de la transaccion
|   correspondiente al id suministrado (transactionID)
|   
|   Recibe: $transactionID = referencia unica de la 
|   transacción
|
|   creó: PG 06-12-17
*/

function getTransactionInformation($transactionID){

        $soapClient = getClientSoap();

        $response   = $soapClient->getTransactionInformation(['auth' => getAuth(), 'transactionID' => $transactionID]);
        return $response;
   
}
