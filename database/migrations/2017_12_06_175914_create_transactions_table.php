<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->increments('id');

            $table->string('documentPayer');
            $table->string('bankCode');
            $table->string('bankInterface');
            $table->string('returnCode');
            $table->string('bankUrl',1000);
            $table->string('trazabilityCode');
            $table->string('transactionCycle');
            $table->string('transactionID');
            $table->string('sessionID');
            $table->string('bankCurrency');
            $table->float('bankFactor');
            $table->integer('responseCode');
            $table->string('responseReasonCode');
            $table->string('responseReasonText');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
