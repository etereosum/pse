<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- bootstrap  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Estilos del desarrollador -->
    <link rel="stylesheet" href="{{asset('css/mis_css.css')}}">


    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- VUE -->
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <!-- VUE AJAX -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.4/vue-resource.min.js"></script>

    <title>@yield('title','ingrese title')</title>
</head>
<body>

    @include('layouts.error')
    @include('flash::message')

    @yield('content','Ingrese el contenido')


    
</body>
</html>