@extends('layouts.main')

@section('title','Start')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a id="btnTransactionList" href="{{route('pagos.index')}}">Ver listado de transacciones</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5">
                <a class="pse" href="{{route('pagos.create')}}">
                    <img src="{{asset('images/pse.png')}}" alt="">  
                    <center>
                        <span>Click para iniciar</span>
                    </center>
                </a> 
            </div>
        </div>
    </div>

    @endsection