@extends('layouts.main')

@section('title','List transactions')

@section('content')

    <div class="container">

        <div id="main">


            <table class="table table-bordered tableTransactions">
                <thead>
                    <tr>
                        <th class="tableHead" colspan="8">
                            Listado de Transacciones
                            <a class="btn btn-default volver" href="{{url('/')}}" role="button">Salir</a>
                        </th>
                    </tr>
                    <tr>
                        <th>    Primary key         </th>
                        <th>    Document Payer      </th>
                        <th>    Bank Code           </th>
                        <th>    Bank Interface      </th>
                        <th>    CUS                 </th>
                        <th>    Transaction ID      </th>
                        <th>    Response reason text</th>
                        <th>    Date                </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in lists">
                        <td>    @{{item.id}}                </td>
                        <td>    @{{item.documentPayer}}     </td>
                        <td>    @{{item.bankCode}}          </td>
                        <td>    @{{item.bankInterface}}     </td>
                        <td>    @{{item.trazabilityCode}}   </td>
                        <td>    @{{item.transactionID}}     </td>
                        <td>    @{{item.responseReasonText}}</td>
                        <td>    @{{item.created_at}}        </td>
                    </tr>    
                </tbody>
            </table>
        </div>
    </div> 


    <script>
       
        var urlTransactions = "{{url('pagos/listTransactions')}}";

        new Vue({
            el: '#main',
            created: function(){
                this.getTransactions();
                this.prueba();
            },
            data : {
                lists: []    
            },
            methods : {
                getTransactions: function(){
                    this.$http.get(urlTransactions).then(function(response){
                        this.lists = response.body.data;
                        console.log(this.lists);
                    },
                    function(){
                        console.log(response.body.data);
                    });
                },
                // se ejecuta por intervalos de tiempo recargando la lista

                prueba:function(){ 
                    setInterval(this.getTransactions,30000);
                }
            }
        });
    </script>

@endsection