@extends('layouts.main')

@section('title','Create Transaction')

@section('content')

<div class="row">
    <div class="col-md-6 col-md-offset-3" id="form">

    <div class="panel panel-default">

        <div class="panel-heading">Ingrese los datos del pagador o trabaje con el usuario por defecto </div>

        <div class="panel-body" id="panel-body">
            <form class="form-horizontal form-label-left"  action="{{route('pagos.transaction')}}" method="POST">
                <div class="form-group">
                    <div class="col-md-12 checkbox">
                        <label>
                            <input type="checkbox" value="1" name="payerDefault" v-model="checked">
                             Deseo trabajar con el usuario por defecto 
                        </label>   
                    </div>
                </div>

                <div v-if="!checked">

                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="">First Name *:</label>
                            <input type="text" class="form-control" name="firstName" value="{{old('firstName')}}">
                        </div>
                        <div class="col-md-6">
                            <label for="">Last Name *:</label>                
                            <input type="text" class="form-control" name="lastName" value="{{old('lastName')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="">Document Type *:</label>                
                            <select class="form-control" name="documentType">
                                <option value="" selected disabled>Selected document</option>
                                @foreach($documentTypes as $type)
                                    <option value="{{$type['abbreviation']}}" {{ (old("documentType") == $type['abbreviation'] ? "selected":"") }}>
                                        {{$type['extended']}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="">Document *:</label>
                            <input type="text" class="form-control" name="document" value="{{old('document')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="">Comapany *:</label>
                            <input type="text" class="form-control" name="company" value="{{old('company')}}">
                        </div>
                        <div class="col-md-6"> 
                            <label for="">Email *:</label>
                            <input type="text" class="form-control" name="emailAddress" value="{{old('emailAddress')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6"> 
                            <label for="">Country *:</label>
                            <input type="text" class="form-control" name="country" value="Colombia" disabled>
                        </div>
                        <div class="col-md-6">           
                            <label for="">Province *:</label>
                            <input type="text" class="form-control" name="province" value="{{old('province')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6"> 
                            <label for="">City *:</label>
                            <input type="text" class="form-control" name="city" value="{{old('city')}}">
                        </div>
                        <div class="col-md-6">
                            <label for="">Address *:</label>
                            <input type="text" class="form-control" name="address" value="{{old('address')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6"> 
                            <label for="">Phone *:</label>
                            <input type="text" class="form-control" name="phone" value="{{old('phone')}}">
                        </div>
                        <div class="col-md-6"> 
                            <label for="">Mobile *:</label>
                            <input type="text" class="form-control" name="mobile" value="{{old('mobile')}}">
                        </div>
                    </div>
                    
                </div>       

            <!-- bankInterface -->

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="">Indique el tipo de cuenta con la cual realizará su pago *:</label>
                        <select class="form-control" name="bankInterface">
                        <option value="" selected disabled>tipo de cuenta</option>
                            
                            @foreach($bankInterfaces as $item)
                                <option value="{{$item['interfaceCode']}}">    
                                    {{$item['interface']}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

            <!-- banks -->

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="">Seleccione de la lista la entidad financiera con la que desea realizar su pago *:</label>
                        <select class="form-control" name="bankCode">
                            @foreach($banks as $bank)
                                <option value="{{$bank->bankCode}}" 
                                    {{  $bank->bankCode == '0' ? 'selected disabled':''}}
                                    {{  (old("bankCode") == $bank->bankCode ? "selected":"")}}
                                    >
                                    {{$bank->bankName}}
                                </option>
                            @endforeach
                        </select>
                    </div>   
                </div>

                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                <center>
                    <button type="submit" class="btn btn-default">Continuar</button>
                </center>

            </form>
        </div>
    </div>

   

    </div>
</div>

<script>
    new Vue({
        el: '#panel-body',
        data:{
            checked: false
        }
    });

</script>

@endsection