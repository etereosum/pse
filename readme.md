# PSE

Funcionalidades:

 * Permite crear transacciones de pago
 * Permite crear transacciones con un usuario de prueba o uno nuevo
 * Permite listar todos los pagos y su estado

### Instalacion
1. Clonar el repositorio por consola: "git clone https://etereosum@bitbucket.org/etereosum/pse.git"
2. Ingresar a la carpeta y actualizar composer con: "composer update"
3. Crear dentro del proyecto un directorio llamado .env y agregar todo el contenido de .env.example para configurar la base de datos y demas elementos
4. Ejecutar por consola el comando "php artisan key:generate" para generar el APP KEY que se encuentra en .env
5. Crear base de datos y agregar la información correspondiente en ".env" ejemplo https://richos.gitbooks.io/laravel-5/content/capitulos/chapter3.html

6. Recuerde que por defecto laravel viene confugurado para trabajar con mySql si desea trabajar con otra base de datos consulte la documentación de laravel. 
7. Ejecutar por consola "php artisan migrate" para generar las migraciones y crear las tablas en la base de datos;
7. Ejecutar "php artisan serve" por consola para iniciar el servidor y correr el aplicativo en un navegador web.
8. Cualquier duda me pueden escribir a etereosum@gmail.com

### Instrucciones funcionales

Vista Inicial:

* En la parte superior "Ver listado de transacciones" permite consultar el listado de las transacciones y su respectivo estado.
    
* En la parte central hay una imagen PSE al darle click llevara al formulario, allí se debe diligenciar la información requerida para la transacción.

Formulario de ingreso de datos

* Al momento de ingresar a la vista se ofrecen dos opciones: "Deseo trabajar con el usuario por defecto" o crear uno nuevo diligenciando el formulario.
* En cualquiera de los dos casos anteriores siempre se debe seleccionar el tipo de banca y el banco (Últimos dos campos del formulario).
* Los campos con asterisco son obligatorios.
* Pulsar continuar para enviar los datos.
* Si se presenta un error del web service o hay un dato erróneo se mostrará un mensaje en la parte superior indicando que sucedió.
* Si todo es correcto Laravel nos llevará a la url de PSE para ingresar con el correo o crear una cuenta, los pasos de esta transacción se encuentran en el documento “Flujo para PSE en pruebas.pdf” suministrado para la prueba.
* Finalmente la página del Banco debe redirigir a la vista donde se listan las transacciones donde la primera transacción de la tabla es la que recién se hizo.


Listado de transacciones

* El listado de transacciones muestra varios campos con la información requerida para validar el estado.
* La columna "CUS" contiene un identificador con el cual PSE nos informa su estado por correo electrónico.
* La columna "Response reason text" contiene texto informativo que indica si la transacción está en espera o aprobada.

* Importante:
  si la transacción está en espera se recomienda mantener la pestaña abierta hasta recibir respuesta, cuando se aprueba la transacción el campo "Response reason text" cambia a "Operación Aprobada"

* Dar click en el botón “Salir” para volver a la vista Inicial.

### Código


La mayoría del código se encuentra documentado por lo tanto indico solo donde están los bloques de código necesario para la ejecución del programa.

* El controlador PagoController es el encargado de gestionar la comunicación entre las vistas, el web service y la base de datos.
* PagoController utiliza el helper pse.php para realizar la mayoría de su lógica, el helper se puede encontrar en app/Helpers/pse.php
* La vistas inicial se carga sobre la view welcome.php (ruta= resources/vies/welcome.php) y el resto de vistas se encuentran en la carpeta resources/views/pagos
* Se utilizó la tecnología Vue, bootstrap 3 y Jquery para el front.
