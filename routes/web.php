<?php


Route::get('/',function(){
    return view('welcome');
});



Route::get('pagos/create',[
    'uses'  => 'PagoController@create',
    'as'    => 'pagos.create'
]);

Route::post('pagos/transaction',[
    'uses'  => 'PagoController@transaction',
    'as'    => 'pagos.transaction'
]);

Route::get('pagos/index', [
    'uses'  => 'PagoController@index',
    'as'    => 'pagos.index'
]);

Route::get('pagos/listTransactions', [
    'uses'  => 'PagoController@listTransactions',
    'as'    => 'pagos.listTransactions'
]);

Route::get('pagos/confirm', [
    'uses'  => 'PagoController@confirm',
    'as'    => 'pagos.confirm'
]);